from django.contrib import admin
from .models import UserProfile,Business

# Register your models here.
admin.site.register(UserProfile)
admin.site.register(Business)
