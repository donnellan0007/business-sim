from django import forms
from django.contrib.auth.forms import UserCreationForm
from app.models import UserProfile,Business
from django.contrib.auth.models import User

class UserProfileForm(UserCreationForm):
    email = forms.EmailField(widget=forms.TextInput(
    attrs={'type': 'email',
           'placeholder':('Email')}))
    class Meta:
        model = User
        fields = ['username','first_name','last_name','email']
        widgets = {
            'username': forms.TextInput(attrs={'placeholder': 'Username'}),
            'first_name': forms.TextInput(attrs={'placeholder': 'First Name'}),
            'last_name': forms.TextInput(attrs={'placeholder': 'Last Name'}),
            'email': forms.TextInput(attrs={'placeholder': 'Email'}),
            
            }
        
    def clean(self):
        cleaned_data = super(UserProfileForm, self).clean()
        name = cleaned_data.get('username')
        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')

class BusinessForm(forms.ModelForm):
    class Meta:
        model = Business
        fields = ['name','description']

    def clean(self):
        cleaned_data = super(BusinessForm,self).clean()
        name = cleaned_data.get('name')
        description = cleaned_data.get('description')