from django.db import models
import uuid
from django.utils.text import slugify
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.urls import reverse
from django.utils import timezone,timesince




# Create your models here.
class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,max_length=30)
    description = models.TextField(max_length=150)

    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.user.username)
        super().save(*args, **kwargs)

@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)
    else:
        instance.userprofile.save()


class Business(models.Model):
    name = models.CharField(max_length=100,unique=True)
    owner = models.ForeignKey(User,on_delete=models.CASCADE)
    description = models.TextField(max_length=400)
    stock_price = models.IntegerField(default=0)
    investors = models.ManyToManyField(User,related_name='investors')

    def get_absolute_url(self):
        return reverse('app:index')

    def __str__(self):
        return self.name
