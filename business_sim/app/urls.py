from django.contrib import admin
from django.urls import path,include
from .views import home,register,logout_view,CreateBusinessView,create_business,room
from . import views
from django.contrib.auth import views as auth_views

app_name = 'app'

urlpatterns = [
    path('chat/',views.home,name='index'),
    path('<str:room_name>/',views.room,name='room'),
    path('user_login/',auth_views.LoginView.as_view(template_name='app/login.html',),name='user_login'),
    path('logout/',views.logout_view,name='logout'),
    path('register/',views.register,name='register'),
    path('create_business/',views.create_business,name='create_business'),
    # path('create_business/',views.CreateBusinessView.as_view(template_name='app/create_business.html'),name='create_business'),
]