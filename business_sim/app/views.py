from django.shortcuts import render
from .models import UserProfile,Business
from .forms import UserProfileForm,BusinessForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.http import HttpResponseRedirect,HttpResponse
from django.urls import reverse,reverse_lazy
from django.shortcuts import render,get_object_or_404,redirect
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
def home(request):
    return render(request,'app/index.html')

def room(request,room_name):
    return render(request,'app/room.html',{'room_name':room_name})

# register
def register(request):
    if request.method == "POST":
        form = UserProfileForm(request.POST)
        if form.is_valid():
            form.save()
            user = request.user
            username = form.cleaned_data.get('username')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')
    else:
        form = UserProfileForm()
    return render(request,'app/register.html',{'form':form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))
        
def create_business(request):
    if request.method == "POST":
        form = BusinessForm(request.POST)
        if form.is_valid():
            business = form.save(commit=False)
            business.owner = request.user
            business.save()
            return redirect('app:index')
    else:
        form = BusinessForm()
    
    return render(request,'app/create_business.html',{'form':form})

class CreateBusinessView(CreateView):
    model = Business
    login_url = reverse_lazy('app:user_login')
    redirect_field_name = 'app/index.html'
    form_class = BusinessForm
    def form_valid(self,form):
        form.instance.owner = self.request.user
        return super().form_valid(form)